package search.common;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.core.environment.EnvironmentSpecificConfiguration.from;

/**
 * Utility class for creating request specifications for API requests.
 * This class provides a static method to build a request specification with base URI, base path, and content type.
 *
 * @author Sathyaraj
 */
public class RequestSpec {

    /**
     * Builds a RequestSpecification with the specified base URI, base path, and content type.
     *
     * @return A RequestSpecification with the configured base URI, base path, and content type.
     */
    public static RequestSpecification searchReqSpec() {
        EnvironmentVariables environmentVariables = Injectors.getInjector()
                .getInstance(EnvironmentVariables.class);

        return new RequestSpecBuilder()
                .setBaseUri(from(environmentVariables).getProperty("baseUrl"))
                .setBasePath(from(environmentVariables).getProperty("basePath"))
                .setContentType(from(environmentVariables).getProperty("contentType"))
                .build();
    }
}
