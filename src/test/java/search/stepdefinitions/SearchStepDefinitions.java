package search.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import search.actionsteps.SearchProductActionStep;
import search.actionsteps.CommonActionStep;

/**
 * Step definitions for searching a product.
 * Contains Cucumber step implementations for actions related to product searching.
 *
 * @author Sathyaraj
 */
public class SearchStepDefinitions {

	protected static String searchProduct;

	@Steps
	public CommonActionStep commonActionStep;

	@Steps
	public SearchProductActionStep searchProductActionStep;

	/**
	 * Sets the product to search.
	 *
	 * @param product The product to search.
	 */
	@Given("I have a product {string} to search")
	public void haveAProduct(String product) {
		searchProduct = product;
	}

	/**
	 * Calls the product search GET endpoint.
	 */
	@When("I call the product search GET endpoint")
	public void callASearchEndPoint() {
		searchProductActionStep.callASearchEndPoint(searchProduct);
	}

	/**
	 * Verifies the success status code in the response.
	 *
	 * @param statusCode The expected success status code.
	 */
	@Then("I receive success status code {int}")
	public void verifySuccessResponseCode(int statusCode) {
		commonActionStep.responseCodeIs(statusCode);
	}

	/**
	 * Verifies that the response contains the searched product.
	 *
	 * @param product The product being searched.
	 */
	@Then("I verify that response contains the searched product {string}")
	public void verifyResultsDisplayedForProduct(String product) {
		searchProductActionStep.validateResponseWithSearchedProduct(product);
	}

	/**
	 * Verifies that the response schema matches the defined schema.
	 *
	 * @param schemaJson The JSON schema for validation.
	 */
	@Then("I validate response schema matches defined in {string}")
	public void verifyResponseSchema(String schemaJson) {
		commonActionStep.verifyResponseSchema(schemaJson);
	}

	/**
	 * Verifies that the response is not empty.
	 */
	@And("I verify response is not empty")
	public void verifyResponseIsNotEmpty() {
		commonActionStep.responseShouldNotBeEmptyList();
	}
}
