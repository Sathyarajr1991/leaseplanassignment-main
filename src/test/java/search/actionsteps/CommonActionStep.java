package search.actionsteps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.core.environment.EnvironmentSpecificConfiguration.from;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Common actions that can be reused for multiple test cases.
 * This class contains common methods for verifying response data and schema.
 *
 * @author Sathyaraj
 */

public class CommonActionStep {
	EnvironmentVariables environmentVariables = Injectors.getInjector()
			.getInstance(EnvironmentVariables.class);
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonActionStep.class);

	/**
	 * Verifies that the response code received is the same as the expected response code.
	 *
	 * @param responseCode the expected response code
	 */
	@Step("Verify that API response is {int}")
	public void responseCodeIs(int responseCode) {
		then().statusCode(responseCode);
	}

	/**
	 * Verifies the response body against a JSON schema.
	 *
	 * This method validates the response body against a specified JSON schema, which is typically used to ensure that the response
	 * structure complies with a predefined schema.
	 *
	 * @param schemaJson The JSON schema file name or path (in classpath) to be used for validation.
	 *                   This should be a JSON schema in the classpath or a path accessible in the environment.
	 *                   For example, if you have a schema file named "schema.json" in the classpath, you can pass "schema.json".
	 * @throws IllegalArgumentException If the specified schemaJson is null or empty.
	 * @throws AssertionError If the response body does not match the specified JSON schema.
	 */
	@Step("Verify response schema with valid schema json")
	public void verifyResponseSchema(String schemaJson) {
		then().body(matchesJsonSchemaInClasspath(from(environmentVariables).getProperty(schemaJson)));
	}

	/**
	 * Verifies that the HTTP response is not an empty list.
	 *
	 * This method checks if the HTTP response contains a non-empty list, ensuring that the response is not empty.
	 *
	 * @throws AssertionError if the response size is zero, indicating an empty list
	 */
	@Step("Verify that response list isn't an empty list")
	public void responseShouldNotBeEmptyList() {
		List<HashMap<String, Object>> res = lastResponse().getBody().jsonPath().getList("$");
		LOGGER.info("Response list size is {}", res.size());
		assertThat(res.size()).as("Response should not be empty").isNotZero();
	}
}
