package search.actionsteps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import io.cucumber.messages.types.Product;
import models.SearchProductModel;
import net.thucydides.core.annotations.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import search.common.RequestSpec;

/**
 * The SearchProductActionStep class contains methods for testing the search product functionality of the API.
 * This class provides steps for calling the search endpoint, verifying search results, and handling errors.
 *
 * @author Sathyaraj
 */
public class SearchProductActionStep {
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchProductActionStep.class);
	int searchProductSize;
	int listHavingUnreachedProducts;

	/**
	 * Calls the search endpoint with the given product parameter.
	 *
	 * @param product the product parameter to send in the search request
	 */
	@Step("Get response from search product {string}")
	public void callASearchEndPoint(String product) {
		RequestSpecification reqSpecification=RequestSpec.searchReqSpec();
		SerenityRest.given().log().uri().spec(reqSpecification).get(product);
	}

	/**
	 * Validates the response for the searched product.
	 *
	 * This method checks if the response contains the specified product by matching it with the "title" field in the JSON response.
	 *
	 * @param displayedProduct The product to be validated in the response.
	 * @throws AssertionError If the count of the specified product in the response is zero.
	 */
	@Step("Verify response for searched product")
	public void validateResponseWithSearchedProduct(String displayedProduct) {
		List<String> titles= getProductTitles(lastResponse().getBody().as(SearchProductModel[].class));
		int jsonSize = titles.size();
		searchProductSize = (int) titles.stream().filter(a -> a.toLowerCase().contains(displayedProduct)).count();
		assertTrue("Count of list in response not having " + displayedProduct + " is zero", searchProductSize > 0);
		listHavingUnreachedProducts = jsonSize - searchProductSize;
		LOGGER.info("Count of list in response not having {} are {} ", displayedProduct, listHavingUnreachedProducts);
	}

	/**
	 * Get the product titles from an array of SearchProductModel.
	 *
	 * @param products An array of SearchProductModel
	 * @return A list of product titles
	 */
	public static List<String> getProductTitles(SearchProductModel[] products) {
		return Arrays.stream(products)
				.map(SearchProductModel::getTitle)
				.collect(Collectors.toList());
	}
}
