package search.runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Test runner class for executing Cucumber scenarios.
 *
 * This class is the entry point for executing Cucumber scenarios using JUnit.
 * It configures Cucumber options such as the plugin and the location of feature files and step definitions.
 *
 * @author Sathyaraj
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features",
        glue="search/stepdefinitions",
        publish=true
)
public class TestRunner {}
