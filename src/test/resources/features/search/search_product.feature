@feature:search 
Feature: Search for the product

  ### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
  ### Available products: "orange", "apple", "pasta", "cola"
  ### Prepare Positive and negative scenarios

  #Search from available products and verify all the response list has searched product 
  @positive
  Scenario Outline: Verifying searched product from available products response
    Given I have a product '<product>' to search
    When I call the product search GET endpoint
    Then I receive success status code <statusCode>
    And I validate response schema matches defined in '<validateSchema>'
    And I verify that response contains the searched product '<product>'

    Examples: 
      | product |statusCode|validateSchema     |
      | apple   |200	   |searchProductSchema|
      | cola    |200	   |searchProductSchema|

#Search for orange product and verify the response is not empty 
  @negative
  Scenario Outline: Verifying available product response is not empty
    Given I have a product '<product>' to search
    When I call the product search GET endpoint
    Then I receive success status code <statusCode>
    And I verify response is not empty

    Examples: 
      | product |statusCode|
      | orange  |200       | 
