**BDD Cucumber with serenity**

*Serenity Cucumber is a framework that combines the behavior-driven development (BDD) approach of Cucumber with the powerful reporting capabilities of Serenity.*

*List of tools used to build framework:*


**FrameWork structure**
- **SearchProductModel.java** : _This class contains all the getters/setters methods for respective endpoints response where We can call the respective members without much time spending on analysing flow structure._
- **TestRunner.java** : *The execution starts from the TestRunner Java file, which is responsible for running the tests using Cucumber and Serenity.*
- **CommonActionStep.java** : *This class contains common steps/methods used for all the step definition classes. These methods are reusable and provide common functionality for different test scenarios.*
- **SearchProductActionStep.java** : *This class contains search related steps/methods which is utilized in all the step definition classes. These methods are reusable and provide search functionality for different test scenarios.*
- **SearchStepDefinitions.java** : *This is a step definition file that defines the step implementations for the BDD scenarios mentioned in the "search_product.features" file. It maps the steps in the feature file to the corresponding step and Java methods.*
- **search_product.features** : *This feature file contains all the test cases or scenarios written in the Gherkin language. Each scenario describes a specific test case for the models product functionality.*
- **schema folder** : *This folder contains the valid schema specification files used for API response schema validation. The schema files are usually in JSON format.*
- **target folder** : *After executing the tests, the Serenity reports are generated and stored in this folder. The reports provide detailed information about the test execution, including test results, logs, and screenshots.*
- **Serenity.conf** : *This configuration file contains environment variables and settings for the framework, such as the base URL for different environments.*

### The project directory structure
![Framework structure image](images/Framework_pic1.PNG)

**How to write test cases ?**
- *Create a new feature file under features folder.*
- *Each Scenario / scenario outline will be considered as one test case. we use scenario outline when we have to add validation under Example tag in the test cases.*
- *Externalize all the global variables, environment specific variables in `Serenity.conf`*
- *Test case is written in Gherkin language with step-definition . Just follow `Given`, `When`,`And`, `Then`*
- *Write common test case method in `Commonactions` and `SearchProductActions`*
- *Tag each test cases to group, like : `@positive or @negative` etc..*

## Executing the tests 
*Run the following command from the root directory:*

```
mvn clean verify
```
*The test results will be recorded here `target/site/serenity/index.html`.*
*Please run the below command from root directory to open the result after execution.*
[images](images)
```bash
open target/site/serenity/index.html 
```
![TestReport image1](images/Report1.PNG)
![TestReport image1](images/Report2.PNG)
![TestReport image1](images/console_report.PNG)

## Gitlab report

- *Go to CI/CD → Pipelines, select the pipeline, and navigate to the test tab to find the online JUnit report.*
- *Go to download artifact and open the Serenity report.*

### Additional configurations

*Additional command line parameters can be passed to switch the application environment.*

*Run the following command:*

```
Run mvn clean verify -Denvironment=dev
```

*Configurations for different environments are set in the test/resources/serenity.conf file. In real-time projects, each environment can be configured with its base URL to run the tests based on different environments.*

```
environments {
  default {
    baseUrl = "https://waarkoop-server.herokuapp.com"
  }
  dev {
    baseUrl = "https://waarkoop-server.herokuapp.com"
  }
  staging {
    baseUrl = "https://waarkoop-server.herokuapp.com"
  }
}
```

###Modifications on framwork are :

- *`Code Restructure` : Added supporting methods and updated steps and java doc for all classes and methods*   
- *`Test suite and test scenarios` : for better coverage*
- *`History` folder is removed*
- *`Build.gradle` is removed(as we have maven )*
- *`gradlew` is removed(as we have maven )*
- *`gradlebat` is removed(as we have maven )* 
- *`serentiy.conf` is updated for different environments*
- *`schema` folder is added for placing json schema files*
- *`Upgraded serenity`: updated to latest serenity features and other libraries in pom.xml file*  


## Requirement in feature file 
- *Please use the endpoint `GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product}` to retrieve the products.*
- *Available products: `orange`, `apple`, `pasta`, `cola`*
- *Prepare positive and negative scenarios*

**Test case implementation explaination:**

## `Scenario 1(Positive case):`
I have verified scenario, after searching the available product, it should return the json with all the titles containing searched product

## `Scenario 2(Negative case):`
Since orange product returns an empty response, hence the validation is added for non-empty json response. 
